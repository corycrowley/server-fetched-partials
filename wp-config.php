<?php
define( 'WP_CACHE', true ) ;
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpjs' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'W`SG_g2pL:-UT=W^11.gm#w7H]V>@[14qWH61-EPXBO-ZH`D^TNYBUA`rD;Ae#&%' );
define( 'SECURE_AUTH_KEY',   ']S}HHMVwt(%0WKAK+P!w9^P2JHfJA$7t{=;`9*#nKK~F}=G~$}PgFu8@b`}RsBo/' );
define( 'LOGGED_IN_KEY',     'od[l53xm|+6hNB~gXL}@OQ<D@`e*&v,729^lJfZ!E_rEj-}Q{w^r]-t&ikrc:{1i' );
define( 'NONCE_KEY',         'TXGH{ 2m[cu3kX~|k:XM(r]/q{JTjVj<w@]vM3^[CZMWC`_wie?f|VE gPZ;CE6&' );
define( 'AUTH_SALT',         'lSKK5jKpDn`6-;VAABfv)P|k+@Hi:+x%8_z4<MgH3#ar8?8c,:Cg#F[Qa2yHP3[&' );
define( 'SECURE_AUTH_SALT',  'Q9yd2G_@UYYx$I{r1l=eQ>Vq^>^;[Ef90Fp9CK[)ma=Iv?W6@ya#|JB*GbZoT=2+' );
define( 'LOGGED_IN_SALT',    '[>%@$i!~m@6s$TyD.X_*TV<)$^FBrQg`mqh&iTJ,_?%9,oXZwW`z=zgpp980}A9 ' );
define( 'NONCE_SALT',        '(T;v&&7q1yMV<UC7bDQxLA#K_126.g,ax0u$3W_K<Q{1fq0}g7T@G`G}v;nO$d7}' );
define( 'WP_CACHE_KEY_SALT', '@Wg/ZTc?dmqxwSz9O^*F/?MY6k-F1h%/d?62BgYO<hr-[XLU5Am`F/y?$NFsE2_:' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


/** Debugger */
define( 'WP_DEBUG', true );

/** Debugger Options */
if ( WP_DEBUG ) {
    /* Tells WordPress to log everything to the /wp-content/debug.log file */
    define( 'WP_DEBUG_LOG', true );

    /* Doesn't force the PHP 'display_errors' variable to be on */
    define( 'WP_DEBUG_DISPLAY', false );

    /* Hides errors from being displayed on-screen */
    @ini_set( 'display_errors', 0 );

    /* Script Debug - default off */
    define( 'SCRIPT_DEBUG', false );

    /* Save Queries - default off */
    define( 'SAVEQUERIES', false );

    /* WC Debug */
    define( 'WCS_DEBUG', true );
}

/* Increase Memory Limit */
define( 'WP_MEMORY_LIMIT', '2048M' );

/* Define Revisions Limit */
define( 'WP_POST_REVISIONS', 3 );

/* Disallow File Edit */
define( 'DISALLOW_FILE_EDIT', true );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
