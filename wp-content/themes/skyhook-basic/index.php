<?php
/**
 * Index Template.
 *
 * @since 1.0.0
 */
get_header();

get_template_part( 'templates/recent-posts' );

get_footer();
