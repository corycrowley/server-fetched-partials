<?php
/**
 * Functions.
 *
 * @since 1.0.0
 */

// Constants.
define( 'SKYHOOK_BASIC_VERSION', '1.0.0' );
define( 'SKYHOOK_BASIC_PATH', trailingslashit( get_template_directory() ) );
define( 'SKYHOOK_BASIC_URL', trailingslashit( get_template_directory_uri() ) );

// Includes.
include SKYHOOK_BASIC_PATH . 'includes/utils.php';
include SKYHOOK_BASIC_PATH . 'includes/setup.php';
include SKYHOOK_BASIC_PATH . 'includes/scripts.php';
include SKYHOOK_BASIC_PATH . 'includes/ajax.php';

/**
 * Get Recent Posts.
 *
 * @since 1.0.0
 *
 * @return WP_Post[]
 */
function skyhook_basic_get_recent_posts() {
	if ( false === ( $posts = get_transient( 'recent_posts' ) ) ) {
		$posts = get_posts( [
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'posts_per_page' => 12,
			'no_found_rows'  => true,
		] );

		set_transient( 'recent_posts', $posts );
	}

	shuffle( $posts );

	return $posts;
}
