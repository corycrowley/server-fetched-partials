<?php
/**
 * Ajax
 *
 * @since 1.0.0
 */

namespace Skyhook\Basic;

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// Hooks
add_action( 'wp_ajax_recent_posts', 'Skyhook\Basic\ajax_recent_posts' );
add_action( 'wp_ajax_nopriv_recent_posts', 'Skyhook\Basic\ajax_recent_posts' );

/**
 * Ajax: Recent Posts.
 *
 * @since 1.0.0
 */
function ajax_recent_posts() {
	get_template_part( 'templates/_partial-recent-posts' );
	die();
}
