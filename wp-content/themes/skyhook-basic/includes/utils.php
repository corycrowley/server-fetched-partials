<?php
/**
 * Utilities.
 *
 * @since 1.0.0
 */

if ( ! function_exists( 'theme_log' ) ) {
	/**
	 * Theme Log.
	 *
	 * @param string $message
	 */
	function theme_log( $message ) {
		if ( WP_DEBUG === true ) {
			if ( is_array( $message ) || is_object( $message ) ) {
				error_log( print_r( $message, true ) );
			} else {
				error_log( $message );
			}
		}
	}
}

if ( ! function_exists( 'get_template_partial' ) ) {
	/**
	 * Get Template Partial.
	 *
	 * @since 1.0.0
	 *
	 * @param string $template
	 */
	function get_template_partial( $template, $vars = [] ) {
		$path = trailingslashit( SKYHOOK_BASIC_PATH . 'templates' );
		$file = "{$path}{$template}.php";

		if ( ! empty( $vars ) ) {
			extract( $vars );
		}

		if ( file_exists( $file ) ) {
			include $file;
		}
	}
}
