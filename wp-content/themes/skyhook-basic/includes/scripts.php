<?php
/**
 * Scripts.
 *
 * @since 1.0.0
 */

namespace Skyhook\Basic;

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// Hooks.
add_action( 'wp_enqueue_scripts', '\Skyhook\Basic\enqueue_scripts', 15 );

/**
 * Enqueue Scripts.
 *
 * @since 1.0.0
 */
function enqueue_scripts() {
	wp_enqueue_style( 'skyhook-basic-tailwind', 'https://unpkg.com/tailwindcss@^1.2/dist/tailwind.min.css', [], SKYHOOK_BASIC_VERSION );
	wp_enqueue_style( 'skyhook-basic-fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css', [], SKYHOOK_BASIC_VERSION );
}
