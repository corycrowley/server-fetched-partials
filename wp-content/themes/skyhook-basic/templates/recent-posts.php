<div class="text-center">
	<h2 class="text-3xl leading-9 tracking-tight font-extrabold text-gray-900 sm:text-4xl sm:leading-10">Recent Posts</h2>
	<p class="mt-3 max-w-2xl mx-auto text-xl leading-7 text-gray-500 sm:mt-4">
		Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsa libero labore natus atque, ducimus sed.
	</p>
	<button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mt-5 rounded" onclick="fetchRecentPosts()">
		<i class="fas fa-random"></i> Randomize
	</button>
</div>

<div class="mt-12 grid grid-cols-3 gap-4" id="js-recent-posts">
	<?php get_template_part( 'templates/_partial-recent-posts' ); ?>
</div>

<script>
	function fetchRecentPosts() {
		fetch( '<?php echo add_query_arg( [ 'action' => 'recent_posts' ], admin_url( 'admin-ajax.php' ) ); ?>' )
		.then( response => response.text() )
		.then( html => {
			document.querySelector( '#js-recent-posts' ).innerHTML = html
		} );
	}
</script>

