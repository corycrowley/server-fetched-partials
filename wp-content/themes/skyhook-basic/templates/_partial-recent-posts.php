<?php foreach ( skyhook_basic_get_recent_posts() as $post ) : ?>
	<div class="max-w-sm rounded overflow-hidden shadow-lg">
		<img class="w-full"
		     src="<?php the_post_thumbnail_url( 'full' ); ?>"
		     alt="<?php the_title(); ?>">

		<div class="px-6 py-4">
			<div class="font-bold text-xl mb-2"><?php echo the_title(); ?></div>
			<p class="text-gray-700 text-base"><?php the_excerpt(); ?></p>
		</div>

		<?php if ( $tags = get_the_tags( get_the_ID() ) ) : ?>
			<div class="px-6 py-4">
				<?php foreach ( $tags as $tag ) : ?>
					<span class="inline-block bg-gray-200 rounded-full px-3 py-1 mb-1 text-sm font-semibold text-gray-700 mr-2">
						<?php printf( '#%s', strtolower( esc_attr( $tag->name ) ) ); ?>
					</span>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
<?php endforeach; ?>
